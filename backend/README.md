# Bitcoin `OP_RETURN` Indexer

Monitor and store `OP_RETURN` outputs and transaction IDs.

## Getting Started

```shell
source ../.env
cargo run
```